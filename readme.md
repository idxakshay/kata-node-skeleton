# NodeJs Skeleton For Kata

## Writing solution for kata

1. Clone the repo and install the dependencies.
2. You can write your solution in ```solution()``` function located in ```src/index.ts``` file. 
3. Test cases can be written in ```src/index.spec.ts``` file.
4. To execute run and test your solution, execute the commands given below.

## Run the project

```sh
npm run start
```

Start in watch mode

```sh
npm run start:watch
```

## Execute test cases

```sh
npm run test
```

Start test in watch mode

```sh
npm run test:watch
```
